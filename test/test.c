#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include "sditf.h"
#include "str_utils.h"

void test_str(struct service * s)
{
    char str[SERVICE_SIZE] = {'\0'};

    printf("\n---\nTest str:\n");

    sditf_service_str(s, str, SERVICE_SIZE);
    printf("%s\n", str);
}

void test_new(struct service * s)
{
    printf("\n---\nTest new:\n");
    sditf_new(SDITF_DBUS_TYPE_SYSTEM, "test-mnt.mount", s);

    system("cat /tmp/test-mnt.mount");
}

void test_remove(void)
{
    printf("\n---\nTest remove:\n");
    sditf_delete(SDITF_DBUS_TYPE_SYSTEM, "test-mnt.mount");
}

int main(int argc, char ** argv)
{
    struct service s = {
        .type = SERVICE_MOUNT,
        .unit = {2, {"Description = coucou", "Documentation=/doc"}},
        .mount = {1, {"What = at"}},
        .install = {1, {"WantedBy = up"}}
    };

    test_str(&s);

    test_new(&s);
    test_remove();

    return 0;
}
