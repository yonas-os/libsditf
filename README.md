# Systemd interface

This aims to provide a systemd interface for the creation and the deletion of
systemd services.

This provides a d-bus interface.

## Requirements

Basically nothing, but it's worthless without systemd.

The d-bus interface requires dbus.

The build requires `meson`, `ninja` or `make`, `gcc` utils or `clang`.

## Build

meson build
cd build
ninja

## Tests

