#ifndef _SDI_DEBUG_H_
#define _SDI_DEBUG_H_

#include <stdlib.h>
#include <stdint.h>

static inline void dump_str(const char * str, size_t len) {
    printf("Dump: ");
    for (; len; --len, ++str)
        printf("%c", *str);
    printf("\n");
}

static inline void dump(const uint8_t * data, size_t len) {
    printf("Dump: ");
    for (; len; --len, ++data)
        printf("%x", *data);
    printf("\n");
}

#endif  // _SDI_DEBUG_H_
