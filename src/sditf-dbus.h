#ifndef _SDITF_DBUS_H_
#define _SDITF_DBUS_H_

#include <stdlib.h>

#include "sditf.h"

#define SDITF_SYSTEM_BUS "org.freedesktop.sditf.system"
#define SDITF_SESSION_BUS "org.freedesktop.sditf.session"

int sditf_dbus_register(enum sditf_dbus_type);
void sditf_dbus_run(void);
void sditf_dbus_quit(void);

#endif  // _SDITF_DBUS_H_
