#ifndef _SDITF_STR_UTILS_H_
#define _SDITF_STR_UTILS_H_

#include <stdlib.h>

#include "sditf.h"

int sditf_service_str(const struct service * service, char * str, size_t str_length);

#endif  // _SDITF_STR_UTILS_H_
