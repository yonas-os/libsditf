#include "sditf.h"

#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "log.h"
#include "str_utils.h"

static int sditf_find_file(enum sditf_dbus_type dbus_type,
                           const char * name, char * file_path) {
    int ret = 0;
    const char * destination = "";

    switch (dbus_type) {
        case SDITF_DBUS_TYPE_SESSION:
            destination = SDITF_SESSION_DEST_DIR;
            break;
        case SDITF_DBUS_TYPE_SYSTEM:
            destination = SDITF_SYSTEM_DEST_DIR;
            break;
    }

    ret = snprintf(file_path, PATH_MAX - 1,
                       "%s/%s", destination, name);
    if (ret < 0) {
        ERROR("sditf_find_file: failed to get file path");
        return -EIO;
    }

    file_path[ret] = '\0';

    return 0;
}

static int sditf_check_service(const struct service * service) {
    return 0;
}

static int sditf_check_service_str(const char * service) {
    return 0;
}


int sditf_new(enum sditf_dbus_type dbus_type,
              const char * name, const struct service * service)
{
    int ret;
    FILE * fd;
    char service_str[SERVICE_SIZE] = {'\0'};
    char service_path[PATH_MAX] = {'\0'};

    if ((name == NULL) || (service == NULL)) {
        DEBUG("sditf_new: input error: name (%p), service (%p)", name, service);
        return -EINVAL;
    }

    ret = sditf_find_file(dbus_type, name, service_path);
    if (ret < 0) {
        ERROR("sditf_new: Failed to cat name (%s) to destination dir", name);
        return -EPERM;
    }

    ret = sditf_check_service(service);
    if (ret < 0) {
        ERROR("sditf_new: invalid service");
        return -EINVAL;
    }

    ret = sditf_service_str(service, service_str, SERVICE_SIZE);
    if (ret < 0) {
        ERROR("sditf_new: failed to export service to string (error %d)", ret);
        return ret;
    }
    service_str[SERVICE_SIZE - 1] = '\0';

    fd = fopen(service_path, "w");
    if (fd == NULL) {
        ERROR("sditf_new: %s: %s(%d)", service_path, strerror(errno), errno);
        fclose(fd);
        return -ENOENT;
    }
    ret = fprintf(fd, service_str);
    if (ret < 0) {
        ERROR("sditf_new: Failed to write service file %s", service_path);
        return -EIO;
    }
    DEBUG("sditf_new: Service written to %s", service_path);
    fclose(fd);

    return 0;
}

int sditf_new_str(enum sditf_dbus_type dbus_type,
                  const char * name, const char * content)
{
    int ret;
    FILE * fd;
    char * service_str = NULL;
    char service_path[PATH_MAX] = {'\0'};

    if ((name == NULL) || (content == NULL)) {
        DEBUG("sditf_new_str: input error: name (%p), content (%p)", name, content);
        return -EINVAL;
    }

    ret = sditf_find_file(dbus_type, name, service_path);
    if (ret < 0) {
        ERROR("sditfa_new: Failed to cat name (%s) to destination dir", name);
        return -EPERM;
    }

    ret = sditf_check_service_str(content);
    if (ret < 0) {
        ERROR("sditf_new_str: invalid service");
        return -EINVAL;
    }

    service_str = strndup(content, SERVICE_SIZE);
    if (service_str == NULL) {
        ERROR("sditf_new_str: failed to copy service content");
        return -ENOMEM;
    }

    fd = fopen(service_path, "w");
    if (fd == NULL) {
        ERROR("sditf_new: %s: %s(%d)", service_path, strerror(errno), errno);
        free(service_str);
        return -ENOENT;
    }
    ret = fprintf(fd, service_str);
    if (ret < 0) {
        ERROR("sditf_new_str: Failed to write service file %s", service_path);
        fclose(fd);
        free(service_str);
        return -EIO;
    }

    DEBUG("sditf_new_str: Service written to %s", service_path);

    fclose(fd);
    free(service_str);

    return 0;
}

int sditf_delete(enum sditf_dbus_type dbus_type,
                 const char * name)
{
    int ret;
    char service_path[PATH_MAX] = {'\0'};

    if (name == NULL) {
        ERROR("sditf_delete: no name given");
    }

    ret = sditf_find_file(dbus_type, name, service_path);
    if (ret < 0) {
        ERROR("sditf_delete: Failed to cat name (%s) to destination dir", name);
        return -EPERM;
    }

    ret = remove(service_path);
    if (ret < 0) {
        WARN("sditf_delete: could not remove file %s (errno %d: %s)",
             service_path, errno, strerror(errno));
    } else {
        DEBUG("sditfx_delete: service %s deleted", name);
    }

    return 0;
}
