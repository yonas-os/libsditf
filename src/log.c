#include "log.h"

#include <libgen.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <syslog.h>
#include <unistd.h>

#include "version.h"

static bool enabled = true;

static const char *program_exec;
static const char *program_path;

const uint8_t LOG_NONE = 8;

#include <stdio.h>

static uint8_t _level_from_opt(uint8_t level) {
    switch (level) {
        case 0: enabled = false; return LOG_NONE;
        case 1: return LOG_ERR;
        case 2: return LOG_WARNING;
        case 3: return LOG_INFO;
        case 4: return LOG_DEBUG;
    }

    return LOG_DEBUG;
}

/**
 * sditf_info:
 * @format: format string
 * @Varargs: list of arguments
 *
 * Output general information
 */
void sditf_info(const char *format, ...)
{
	va_list ap;

        if (enabled) {
            va_start(ap, format);

            vsyslog(LOG_INFO, format, ap);

            va_end(ap);
        }
}

/**
 * sditf_warn:
 * @format: format string
 * @Varargs: list of arguments
 *
 * Output warning messages
 */
void sditf_warn(const char *format, ...)
{
	va_list ap;

        if (enabled) {
            va_start(ap, format);

            vsyslog(LOG_WARNING, format, ap);

            va_end(ap);
        }
}

/**
 * sditf_error:
 * @format: format string
 * @varargs: list of arguments
 *
 * Output error messages
 */
void sditf_error(const char *format, ...)
{
	va_list ap;

        if (enabled) {
            va_start(ap, format);

            vsyslog(LOG_ERR, format, ap);

            va_end(ap);
        }
}

/**
 * sditf_debug:
 * @format: format string
 * @varargs: list of arguments
 *
 * Output debug message
 *
 * The actual output of the debug message is controlled via a command line
 * switch. If not enabled, these messages will be ignored.
 */
void sditf_debug(const char * format, ...)
{
	va_list ap;

        if (enabled) {
            va_start(ap, format);

            vsyslog(LOG_DEBUG, format, ap);

            va_end(ap);
        }
}

/**
 * sditf_log_init:
 * @program: name of the program
 * @level: log level
 * @detach: true if process is suuposed to be detached
 *
 * Initialize logs
 */
int sditf_log_init(char * program, uint8_t level,
                   bool detach)
{
	static char path[PATH_MAX];
	int option = LOG_CONS | LOG_PID | LOG_NDELAY;

	program_exec = program;
	program_path = getcwd(path, sizeof(path));

        uint8_t log_level = _level_from_opt(level);
        if (log_level == LOG_NONE) {
            return 0;
        }

        setlogmask(LOG_UPTO (log_level));

	openlog(basename(program), option, detach ? LOG_DAEMON : LOG_USER);

	syslog(LOG_INFO, "sditf version %s", VERSION);

	return 0;
}

/**
 * sditf_log_cleanup:
 *
 * Terminate logs
 */
void sditf_log_cleanup(void)
{
	syslog(LOG_INFO, "Exit");

	closelog();
}
