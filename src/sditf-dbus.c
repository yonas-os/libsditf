#include "sditf-dbus.h"

#include <ell/ell.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "sditf.h"

static void do_debug(const char *str, void * user_data)
{
    const char * prefix = user_data;

    l_info("%s%s", prefix, str);
}

static void signal_handler(uint32_t signo, void * user_data)
{
    switch (signo) {
	case SIGINT:
	case SIGTERM:
            l_info("Terminate");
            l_main_quit();
            break;
    }
}

static void request_name_callback(struct l_dbus *dbus, bool success,
                                  bool queued, void * user_data)
{
    l_info("request name result=%s",
           success ? (queued ? "queued" : "success") : "failed");
}

static void ready_callback(void * user_data)
{
    l_info("ready");
}

static void disconnect_callback(void * user_data)
{
    l_main_quit();
}

static struct l_dbus_message * sditf_dbus_create_service(struct l_dbus *dbus,
                                                         struct l_dbus_message * message,
                                                         void * user_data)
{
    struct l_dbus_message * reply;
    const char * service_name = NULL, * service_content = NULL;
    const char * interface_name = l_dbus_message_get_interface(message);
    enum sditf_dbus_type sditf_dbus_type = SDITF_DBUS_TYPE_SYSTEM;

    l_info("Create service");

    /* Check if request comes from the session bus or the system bus */
    if (strcmp(interface_name, SDITF_SYSTEM_BUS) == 0) {
        sditf_dbus_type = SDITF_DBUS_TYPE_SYSTEM;
    } else if (strcmp(interface_name, SDITF_SESSION_BUS) == 0) {
        sditf_dbus_type = SDITF_DBUS_TYPE_SESSION;
    }

    l_dbus_message_get_arguments(message, "ss", &service_name, &service_content);

    int ret = sditf_new_str(sditf_dbus_type, service_name, service_content);
    if (ret < 0) {
        l_error("Failed to create service");
        reply = l_dbus_message_new_method_return(message);
        l_dbus_message_set_arguments(reply, "s", "failed");
        return reply;
    }

    reply = l_dbus_message_new_method_return(message);
    l_dbus_message_set_arguments(reply, "s", "ok");

    return reply;
}

static void setup_sditf_interface(struct l_dbus_interface *interface)
{
    l_dbus_interface_method(interface, "CreateService", 0,
                               sditf_dbus_create_service, "s", "ss",
                               "return_status", "service_name", "service_content");
}

int sditf_dbus_register(enum sditf_dbus_type dbus_type)
{
    int ret = 0;
    struct l_dbus * dbus = NULL;
    const char * bus_name = SDITF_SESSION_BUS;
    enum l_dbus_bus bus_type = L_DBUS_SESSION_BUS;

    if (!l_main_init())
        return -1;

    l_log_set_stderr();

    switch (dbus_type) {
        case SDITF_DBUS_TYPE_SESSION:
            bus_type = L_DBUS_SESSION_BUS;
            bus_name = SDITF_SESSION_BUS;
            break;
        case SDITF_DBUS_TYPE_SYSTEM:
            bus_type = L_DBUS_SYSTEM_BUS;
            bus_name = SDITF_SYSTEM_BUS;
            break;
    }

    dbus = l_dbus_new_default(bus_type);
    if (dbus == NULL) {
        l_info("Unable to create dbus interface: %s(%d)",
               strerror(errno), errno);
        ret = -EBADE;
        goto cleanup;
    }

    l_dbus_set_debug(dbus, do_debug, "[DBUS] ", NULL);
    l_dbus_set_ready_handler(dbus, ready_callback, dbus, NULL);
    l_dbus_set_disconnect_handler(dbus, disconnect_callback, NULL, NULL);

    l_dbus_name_acquire(dbus, bus_name, false, false, false,
                        request_name_callback, NULL);

    if (!l_dbus_object_manager_enable(dbus, "/")) {
        l_info("Unable to enable Object Manager");
        ret = -EBADE;
        goto cleanup;
    }

    if (!l_dbus_register_interface(dbus, bus_name, setup_sditf_interface,
                                   NULL, true)) {
        l_info("Unable to register interface");
        ret = -EBADE;
        goto cleanup;
    }

    if (!l_dbus_object_add_interface(dbus, "/", bus_name, NULL)) {
        l_info("Unable to instantiate interface");
        ret = -EBADE;
        goto cleanup;
    }

    return 0;

cleanup:
    l_dbus_destroy(dbus);
    l_main_exit();

    return ret;
}

void sditf_dbus_run(void)
{
    l_main_run_with_signal(signal_handler, NULL);
}

void sditf_dbus_quit(void)
{
    l_main_quit();
}
