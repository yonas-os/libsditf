#ifndef _SDI_LOG_H_
#define _SDI_LOG_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

int sditf_log_init(char * program, uint8_t level,
                   bool detach);
void sditf_log_cleanup(void);

void sditf_info(const char *format, ...);
void sditf_warn(const char *format, ...);
void sditf_error(const char *format, ...);
void sditf_debug(const char * format, ...);

#define INFO  sditf_info
#define WARN  sditf_warn
#define ERROR sditf_error
#define DEBUG sditf_debug

#endif  // _SDI_LOG_H_
