#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "sditf-dbus.h"

#include "config.h"
#include "log.h"
#include "sditf.h"
#include "version.h"

static void usage(const char * program_name)
{
    printf("%s - Wireless daemon\n"
           "Usage:\n", program_name);
    printf("\t%s [options]\n", program_name);
    printf("Options:\n"
#if CONFIG_SESSION_BUS == 1
           "\t--session              Enable session bus\n"
#endif
#if CONFIG_SYSTEM_BUS == 1
           "\t--system               Enable system bus\n"
#endif
           "\t-v, --version          Increase log level (up to three)\n"
           "\t-q, --quiet            Disable logging\n"
           "\t-V, --version          Show version\n"
           "\t-h, --help             Show help options\n");
}

static const struct option main_options[] = {
    { "detach",    no_argument,       NULL, 'd' },
#if CONFIG_SESSION_BUS == 1
    { "session",   no_argument,       NULL, 's' },
#endif
#if CONFIG_SYSTEM_BUS == 1
    { "system",    no_argument,       NULL, 'S' },
#endif
    { "verbose",   no_argument,       NULL, 'v' },
    { "quiet",     no_argument,       NULL, 'q' },
    { "version",   no_argument,       NULL, 'V' },
    { "help",      no_argument,       NULL, 'h' },
    { }
};

int main(int argc, char ** argv)
{
    int ret = 0;
    uint8_t opt_loglevel = 1;
    bool opt_session = false, opt_system = false, opt_detach = false;

    int opt;
    do {
        opt = getopt_long(argc, argv, "sSdqvhV",
                          main_options, NULL);

        switch(opt) {
            case 'd':
                opt_detach = true;
                break;
#if CONFIG_SESSION_BUS == 1
            case 's':
                opt_session = true;
                break;
#endif
#if CONFIG_SYSTEM_BUS == 1
            case 'S':
                opt_system = true;
                break;
#endif
            case 'v':
                ++opt_loglevel;
                break;
            case 'q':
                opt_loglevel = 0;
                break;
            case 'V':
                printf("%s\n", VERSION);
                return EXIT_SUCCESS;
            case 'h':
                usage(argv[0]);
                return EXIT_SUCCESS;
            case -1:
                break;
            default:
                usage(argv[0]);
                return EXIT_FAILURE;
        }
    } while (opt != -1);

    sditf_log_init(argv[0], opt_loglevel, opt_detach);

    if (opt_session == 1) {
        ret = sditf_dbus_register(SDITF_DBUS_TYPE_SESSION);
        if (ret < 0) {
            ERROR("main: could not register for dbus");
            return ret;
        }
    }

    if (opt_system == 1) {
        ret = sditf_dbus_register(SDITF_DBUS_TYPE_SYSTEM);
        if (ret < 0) {
            ERROR("main: could not register for dbus");
            return ret;
        }
    }

    if ((opt_session == 1) || (opt_system == 1)) {
        sditf_dbus_run();

        sditf_dbus_quit();
    }

    sditf_log_cleanup();

    return EXIT_SUCCESS;
}
