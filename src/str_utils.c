#include "str_utils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "log.h"
#include "sditf.h"

#include "debug.h"

/**
 * Transform a section into a string
 *
 * Upon successful return, this function returns the number of character written
 * into @str (excluding the null terminating byte) . On failure it returns an
 * negative error.
 **/
static int sditf_service_str_section(const struct section * section,
                                     char * str, size_t str_length)
{
    int ret, w_length = 0;
    size_t it = 0;

    if ((section == NULL) || (str == NULL) || (str_length == 0)) {
        ERROR("sditf_service_str_section: incorrect input");
        return -EINVAL;
    }

    while ((it < MAX_SETTINGS_PER_SECTION)
           && (section->settings[it][0] != '\0')) {
        ret = snprintf(str + w_length, str_length - 1 - w_length,
                       "%s\n", section->settings[it]);
        if (ret < 0) {
            ERROR("sditf_service_str_section: failed to write setting %zu", it);
            return -EIO;
        }

        w_length += ret;
        ++it;
    }

    return w_length;
}

/**
 * Transform a secrive into a string
 *
 * Upon successful return, this function returns the number of character written
 * into @str (excluding the null terminating byte) . On failure it returns an
 * negative error.
 **/
int sditf_service_str(const struct service * service,
                      char * str, size_t str_length)
{
    int ret, w_length = 0;
    const char * specific_header = NULL;
    const struct section * specific_section = NULL;

    if ((service == NULL) || (str == NULL) || (str_length == 0)) {
        ERROR("sditf_service_str: incorrect input");
        return -EINVAL;
    }

    ret = snprintf(str, str_length - 1,
                   "This file is auto-generated. Do not modify.\n\n"
                   "[Unit]\n"
                   );
    if (ret < 0) {
        ERROR("sditf_service_str: failed to write unit header");
        return -EIO;
    }
    w_length += ret;

    ret = sditf_service_str_section(&service->unit, str + w_length, str_length - w_length);
    if (ret < 0) {
        ERROR("sditf_service_str: failed to copy unit section");
        return -EIO;
    }
    w_length += ret;
    str[w_length++] = '\n';

    switch (service->type) {
        case SERVICE_MOUNT:
            specific_header = "[Mount]";
            specific_section = &service->mount;
            break;
        default:
            ERROR("sditf_service_str: not implemented");
            return -ENOSYS;
    }

    ret = snprintf(str + w_length, str_length - w_length - 1,
                   "%s\n", specific_header);
    if (ret < 0) {
        ERROR("sditf_service_str: failed to write specific header");
        return -EIO;
    }
    w_length += ret;

    ret = sditf_service_str_section(specific_section,
                                    str + w_length, str_length - w_length);
    if (ret < 0) {
        ERROR("sditf_service_str: failed to copy specific section");
        return -EIO;
    }
    w_length += ret;
    str[w_length++] = '\n';

    ret = snprintf(str + w_length, str_length - w_length - 1,
                   "[Install]\n");
    if (ret < 0) {
        ERROR("sditf_service_str: failed to write install header");
        return -EIO;
    }
    w_length += ret;

    ret = sditf_service_str_section(&service->install,
                                    str + w_length, str_length - w_length);
    if (ret < 0) {
        ERROR("sditf_service_str: failed to copy install section");
        return -EIO;
    }
    w_length += ret;

    str[str_length - 1] = '\0';
    return w_length;
}
