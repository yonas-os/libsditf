# To do

## Tests

Include a test framework

## D-bus interface

Create a d-bus interface.

## Main

Create a main with a daemon mode loading the d-bus interface, and a normal mode
using arguments.

Create a systemd service to manage the daemon.

## Services location

Define a service location. Refer to `man systemd.unit`.

## Documentation

Comment the functions

