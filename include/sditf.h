#ifndef _SDITF_SDITF_H_
#define _SDITF_SDITF_H_

#include <stdlib.h>

#define SETTING_LENGTH 100
#define MAX_SETTINGS_PER_SECTION 10
#define SERVICE_SIZE 1000

typedef char Setting[SETTING_LENGTH];

enum sditf_dbus_type {
    SDITF_DBUS_TYPE_SESSION,
    SDITF_DBUS_TYPE_SYSTEM
};

enum service_type {
    SERVICE_UNIT = 0,
    SERVICE_MOUNT,
    SERVICE_SOCKET,
    SERVICE_AUTOMOUNT
};

struct section {
    size_t n_settings;
    Setting settings[MAX_SETTINGS_PER_SECTION];
};

struct service {
    enum service_type type;

    struct section unit;
    union {
        struct section mount;
    };
    struct section install;
};

struct service_status {

};

int sditf_new(enum sditf_dbus_type dbus_type,
              const char * name, const struct service * service);
int sditf_new_str(enum sditf_dbus_type dbus_type,
                  const char * name, const char * content);
int sditf_delete(enum sditf_dbus_type dbus_type,
                 const char * name);


#endif  // _SDITF_SDITF_H_
